#!/bin/bash

echo '
      GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"
     ' >> /etc/default/grub

update-grub

echo '
      auto eth0
      allow-hotplug eth0
      iface eth0 inet dhcp
     ' >> /etc/network/interfaces
